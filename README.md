Playbook linux-gitlab-runner :
=========

This playbook install docker on inventory and create gitlab runners.
Runner are "`linux runners`"

Requirement :pushpin:
------------
A list of roles requirements

- **role** : [docker:>=4.2](https://galaxy.ansible.com/geerlingguy/docker)
- **role** : [gitlab-runner-install:>=v1.0](https://gitlab.com/infrastructure-as-code6/ansible/roles/ansible-role-gitlab-runner-install)
- **role** : [gitlab-runner:>=v2.1](https://gitlab.com/infrastructure-as-code6/ansible/roles/ansible-role-linux-gitlab-runner)





Launcher playbook :video_game:
------------
:unlock:  **vault password** :unlock:  : `helloworld` 

Install requirement : 
```cmd
ansible-galaxy install --roles-path roles -r requirements.yml
```
Launch playbook : 
```cmd
ansible-playbook --ask-vault-pass -i inventory.yml playbook.yml
```


Configure your environment :wrench:
------------

- **Edit inventory**:

In "`./inventory.yml` file, you can edit the `hosts` property.

- **Edit ssh file location** :

In "`./groups_vars/debian_servers/main.yml` file, you can edit `ansible_ssh_private_key_file` variable


- **Edit vault informations** :

In "`./groups_vars/all/vault.yml` file, you can edit `runner_registration_token` variable

```cmd
ansible-vault edit ./group_vars/all/vault.yml
```


- **Edit playbook configuration** :

To finish, use "`./groups_vars/all/common.yml` to define/override roles variables.
( View roles variables in each role repository)
